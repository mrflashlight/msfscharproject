// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickups/PickupItem.h"
#include "HealthPickup.generated.h"

/**
 * 
 */
UCLASS()
class MRFSCHARPROJECT_API AHealthPickup : public APickupItem
{
	GENERATED_BODY()
	
public:
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	UPROPERTY(EditDefaultsOnly)
		float RecoverHealth = 50.f;

	UFUNCTION()
		void HealthPickup(UPrimitiveComponent* OverlapperComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
