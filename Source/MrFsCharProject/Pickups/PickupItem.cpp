// Fill out your copyright notice in the Description page of Project Settings.


#include "PickupItem.h"
#include "GameFramework/Character.h"
#include "Components/BoxComponent.h"
#include "Components/PrimitiveComponent.h"

// Sets default values
APickupItem::APickupItem()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	ItemMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemMesh"));
	ItemMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	RotationRate = FRotator(0.0, 180.0, 0.0);

	Speed = 1.0;

	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	BoxCollider->SetGenerateOverlapEvents(true);
	BoxCollider->SetWorldScale3D(FVector(1.0, 1.0, 1.0));
	BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &APickupItem::OnOverlapBegin);
	BoxCollider->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
}



// Called when the game starts or when spawned
void APickupItem::BeginPlay()
{
	Super::BeginPlay();

}


// Called every frame
void APickupItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorLocalRotation(RotationRate * DeltaTime * Speed);

}

void APickupItem::OnOverlapBegin(UPrimitiveComponent* OverlapperComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Check if the OtherActor is not me and if th is not Null
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComponent != nullptr))
	{
// 		FString Pickup = FString::Printf(TEXT("Picked up: %s"), *GetName());
// 
// 		GEngine->AddOnScreenDebugMessage(1, 5, FColor::Red, Pickup);
// 
		Destroy();
	}


}

