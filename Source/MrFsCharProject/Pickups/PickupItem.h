// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickupItem.generated.h"

UCLASS()
class MRFSCHARPROJECT_API APickupItem : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APickupItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
		FRotator RotationRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
		class USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
		class UStaticMeshComponent* ItemMesh;

	//Rotation speed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
		float Speed;

	// Box Collider and Overlap Function
	UPROPERTY(EditAnywhere)
		class UBoxComponent* BoxCollider;

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlapperComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
