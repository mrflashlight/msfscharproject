// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/Base_Weapon.h"
#include "Weapon_Projectile.generated.h"

/**
 * 
 */
UCLASS(abstract)
class MRFSCHARPROJECT_API AWeapon_Projectile : public ABase_Weapon
{
	GENERATED_BODY()

			
protected:
	virtual void Fire() override;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class ABAse_Projectile> ProjectileClass;
						
public:
	virtual void Recharge() override;

};
