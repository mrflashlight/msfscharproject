// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/Base_Weapon.h"
#include "Weapon_Trace.generated.h"

/**
 * 
 */
UCLASS(abstract)
class MRFSCHARPROJECT_API AWeapon_Trace : public ABase_Weapon
{
	GENERATED_BODY()
	
public:
	virtual void Fire() override;

	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float TraceLength = 200;

	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float SphereRadius = 50;
};
