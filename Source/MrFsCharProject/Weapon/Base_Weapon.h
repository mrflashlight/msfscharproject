// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Base_Weapon.generated.h"


class UAnimMontage;
class UTexture;

UCLASS(abstract)
class MRFSCHARPROJECT_API ABase_Weapon : public AStaticMeshActor
{
	GENERATED_BODY()

public:

	ABase_Weapon();

	virtual void StartFire();
	virtual void StopFire();

protected:

	UPROPERTY(VisibleAnywhere)
		class UImpactEffectComponent* ImpactEffectComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Muzzle")
		FName MuzzleSocketName;

	//Shots per min
	UPROPERTY(EditDefaultsOnly, Category = "Autofire", meta = (EditCondition = "bAutoFire"))
		float FireRate = 250;
	UPROPERTY(EditDefaultsOnly, Category = "Autofire")
		bool bAutoFire = false;

	UPROPERTY()
		class ACharacter* WeaponOwner;

	FTimerHandle AutoFire_TH;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
		int32 LoadedAmmo = 30;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
		int32 AmmoPool = 30;
		

public:
	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* FireMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* RechargeMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Images")
		UTexture* Image;

	UFUNCTION()
	virtual void Fire();

	UFUNCTION()
	virtual void Recharge();
	
	FTransform GetMuzzleTransform();

	virtual void BeginPlay() override;

};
