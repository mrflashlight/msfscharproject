// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/Base_Weapon.h"
#include "ThrowableWeapon.generated.h"

/**
 * 
 */
UCLASS()
class MRFSCHARPROJECT_API AThrowableWeapon : public ABase_Weapon
{
	GENERATED_BODY()

public:

	AThrowableWeapon();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* WeaponMesh;

	UFUNCTION(BlueprintCallable)
		void ThrowProjectile();
	
protected:

	void UseWeapon();

	void StopUseWeapon();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config", meta = (AllowPrivateAccess = "true"))
		TSubclassOf<class ABAse_Projectile> ThrowProjectileClass;

	UPROPERTY()
		FTimerHandle ThrowProjectileTimer;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config", meta = (AllowPrivateAccess = "true"))
		uint32 bMustHoldToThrow : 1;

	/** hold delay between throw  */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config", meta = (AllowPrivateAccess = "true"))
		float HoldTime = 1.f;
};
