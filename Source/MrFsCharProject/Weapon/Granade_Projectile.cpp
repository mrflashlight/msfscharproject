// Fill out your copyright notice in the Description page of Project Settings.


#include "Granade_Projectile.h"
#include "Particles/ParticleSystem.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Sound/SoundBase.h"
#include "ConstructorHelpers.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"

AGranade_Projectile::AGranade_Projectile()
{
// 	ProjectileCollision->BodyInstance.bSimulatePhysics = true;
// 	ProjectileMesh->SetRelativeScale3D(FVector(0.05f, 0.05f, 0.07f));
// 	MovementComponent->InitialSpeed = 1000.f;
// 	MovementComponent->MaxSpeed = 1000.f;
// 	MovementComponent->bRotationFollowsVelocity = false;
// 	MovementComponent->ProjectileGravityScale = 1.f;
// 	MovementComponent->bShouldBounce = true;
// 
// 	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleSystem(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));
// 	if (ParticleSystem.Object)
// 	{
// 		EmitterTemplate = ParticleSystem.Object;
// 	}

	/** set the sound asset  */
// 	static ConstructorHelpers::FObjectFinder<USoundBase> EXplosionSoundTemplate(TEXT("SoundCue'/Game/StarterContent/Audio/Explosion_Cue.Explosion_Cue'"));
// 	if (EXplosionSoundTemplate.Object)
// 	{
// 		ExplodeSound = EXplosionSoundTemplate.Object;
// 	}
}

void AGranade_Projectile::BeginPlay()
{
	Super::BeginPlay();
	
	/** init grenade speed  */
// 	ProjectileCollision->AddImpulse(GetActorForwardVector() * ImpulseStrength);
// 	FVector Impulse;
// 	Impulse.X = FMath::FRandRange(-5.f, 5.f);
// 	Impulse.Y = FMath::FRandRange(-5.f, 5.f);
// 	Impulse.Z = FMath::FRandRange(-5.f, 5.f);
// 	Impulse *= 500.f;
// 	ProjectileCollision->AddAngularImpulseInDegrees(Impulse);

	/** explode by timer  */
// 	FTimerHandle ExplodeTimer;
// 	GetWorld()->GetTimerManager().SetTimer(ExplodeTimer, this, &AGranade_Projectile::Explode, TimeToExplode, false);
}

// void AGranade_Projectile::OnExploded()
// {
// 	if (GetWorld())
// 	{
// 		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), EmitterTemplate, GetActorLocation());
// 		UGameplayStatics::PlaySoundAtLocation(this, ExplodeSound, GetActorLocation(), 1.f, 1.f, 0.f);
// 	}
// }

// void AGranade_Projectile::Explode()
// {
// 	bExploded = true;
// 
// 	TArray<AActor*> IgnoredActors;
// 	IgnoredActors.Add(this);
// 
// 	SetActorHiddenInGame(true);
// 	SetActorEnableCollision(false);
// 	SetLifeSpan(10.f);
// }
