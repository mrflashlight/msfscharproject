// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon_Projectile.h"
#include "BAse_Projectile.h"
#include "ImpactEffectComponent.h"


void AWeapon_Projectile::Fire()
{
	Super::Fire();

	if (ProjectileClass)
	{
		FTransform TmpTransform = GetMuzzleTransform();

		ABAse_Projectile* TmpProjectile = Cast<ABAse_Projectile>(GetWorld()->SpawnActor(ProjectileClass, &TmpTransform));

		TmpProjectile->OnHitFire.BindDynamic(ImpactEffectComponent, &UImpactEffectComponent::SpawnImpactEffect);
	}


}

void AWeapon_Projectile::Recharge()
{
	Super::Recharge();
}
