// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Animation/AnimMontage.h"
#include "Animation/AnimInstance.h"
#include "Pickups/PickupItem.h"
#include "MyCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMyNoParamDelegate);

class UCameraComponent;
class USpringArmComponent;
class UAnimInstance;
class UAnimMontage;
class USceneComponent;


UCLASS()
class MRFSCHARPROJECT_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForvard(float Scale);
	void MoveRight(float Scale);
	void Jog();
	void Walk();
	void StartSprint();
	void StopSprint();
	void ZoomIn();	
	void ZoomOut();
	void StartFire();
	void StopFire();
	void RechargeOn();
	void AttachWeapon(FName SocketName);
// 	void DropWeapon();
	void EquipWeapon();
	void UnEquipWeapon();

	UFUNCTION()
		void CharacterDied();

	class ABase_Weapon* SpawnWeapon();


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USpringArmComponent* SpringArmComponent;

	UPROPERTY(VisibleAnywhere, Category = "Animation")
		UAnimInstance* AnimInstance;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Montage")
		UAnimMontage* Fire_Rifle_Hip1_Add_Montage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Montage")
		UAnimMontage* Recharge_Rifle_Hip_Montage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Montage")
		UAnimMontage* EquipWeapon_Add_Montage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Montage")
		UAnimMontage* UnEquipWeapon_Add_Montage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SceneComponent")
		USceneComponent* WeaponComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character Movement: Walking")
		float MaxWalkSpeed;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character Movement: Walking")
		float JogSpeed;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character Movement: Walking")
		float SprintSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float Zoom_v;

	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		TSubclassOf<class ABase_Weapon> WeaponToSpawn_Class;

	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		FName WeaponSocket;
	
	UPROPERTY(BlueprintReadOnly)
		class ABase_Weapon* CurrentWeapon;


	UPROPERTY()
		UAnimMontage* FireMontage;

	UPROPERTY()
		UAnimMontage* RechargeMontage;
	
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
 		FMyNoParamDelegate OnAmmoPoolChanged;

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FMyNoParamDelegate OnRecharged;

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FMyNoParamDelegate OnWeaponChanged;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UVitalityComponent* VitalityComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
		int32 LoadedAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
		int32 AmmoPool;

	void RotateToMouse();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	bool bJog;
	bool bFirePressed;
	bool bRechargeOn;
	bool bWeaponEquiped;
	bool bWeaponUnEquiped;
// 	bool bDropWeapon;
};
