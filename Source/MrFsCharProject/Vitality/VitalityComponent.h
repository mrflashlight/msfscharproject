// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VitalityComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegate);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MRFSCHARPROJECT_API UVitalityComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UVitalityComponent();

	UFUNCTION()
		void StopRegenHP();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
		void DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION()
		void RegenHP();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health/HP")
		float MaxHP = 100;

	UPROPERTY(BlueprintReadOnly, Category = "Health/HP")
		float CurrentHP = MaxHP;

 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Score")
		int Score = 100;

	//Enable RegenHP
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health/HP")
		bool bCanRegenHP = true;

	//HP to Regen
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health/HP", meta = (EditCondition = "bCanRegenHP"))
		float RegenHPRate = 5;

	//Starts RegenHP in N sec since damaged
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health/HP", meta = (EditCondition = "bCanRegenHP"))
		float RegenHPDelay = 2;

	FTimerHandle AutoRegenHP_TH;

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
	FNoParamDelegate OnHealthChanged;


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

// 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health/HP")
// 		bool bAlive = true;

	UFUNCTION()
		float GetMaxHP();

	UFUNCTION()
		float GetCurrentHP();

	UFUNCTION()
		void SetCurrentHP(float HP);

	UFUNCTION(BlueprintCallable)
		bool IsAlive();
	
	UPROPERTY(BlueprintAssignable, Category = "Delegates")
	FNoParamDelegate OnOwnerDied;
};
