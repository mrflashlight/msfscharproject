// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MrFs_GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MRFSCHARPROJECT_API UMrFs_GameInstance : public UGameInstance
{
	GENERATED_BODY()
	
private:

	int CurrentScore = 0;

public:

	UFUNCTION(BlueprintCallable)
	int GetCurrentScore() const; 

	void AddScore(int ScoreToAdd);

	void FlushCurrentScore();

};
